using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGeneration : MonoBehaviour
{
    [Header("[Tile Atlas]")]
    public TileAtlas tileAtlas;

    [Header("[World Gen Settings]")]
    public int chunkSize = 16;
    public bool generateCaves = true;
    public int dirtLayerHeight = 5;
    public float surfaceValue = .25f;
    public int worldSize = 128;
    public float heightMultiplier = 4f;
    public int heightAddition = 25;

    [Header("[Tree Settings]")]
    public int treeChance = 10;
    public int minTreeHeight = 4;
    public int maxTreeHeight = 6;

    [Header("[Noise Settings]")]
    public float caveFreq = 0.05f;
    public float terrainFreq = 0.04f;
    public float seed; //default is 0 but L39 randomises it
    public Texture2D noiseTexture;

    private GameObject[] worldChunks;
    private List<Vector2> worldTiles = new List<Vector2>();

    private void Start()
    {
        seed = Random.Range(-50000, 50000);
        GenerateNoiseTexture();
        CreateChunks();
        GenerateTerrain();
    }

    public void CreateChunks()
    {
        int numChunks = worldSize / chunkSize;
        worldChunks = new GameObject[numChunks];
        for (int i = 0; i< numChunks; i++)
        {
            GameObject newChunk = new GameObject();
            newChunk.name = i.ToString();
            newChunk.transform.parent = this.transform;
            worldChunks[i] = newChunk;
        }

    }

    public void GenerateTerrain()
    {
        for (int x = 0; x < worldSize; x++)
        {
            float height = Mathf.PerlinNoise((x + seed) * terrainFreq, seed * terrainFreq) * heightMultiplier + heightAddition;
            for (int y = 0; y < height; y++)
            {
                Sprite tileSprite;
                if (y < height - dirtLayerHeight)
                {
                    tileSprite = tileAtlas.stone.tileSprite; 
                }
                else if(y < height - 1)
                {
                    tileSprite = tileAtlas.dirt.tileSprite;
                }
                else
                {
                    tileSprite = tileAtlas.grass.tileSprite;
                }
                if (generateCaves)
                {
                    if (noiseTexture.GetPixel(x, y).r > surfaceValue)
                    {
                        PlaceTile(tileSprite, x, y);
                    }
                }
                else
                {
                    PlaceTile(tileSprite, x, y);
                }
                if (y >= height - 1)
                {
                    int t = Random.Range(0, treeChance);
                    if (t == 1)
                    {
                        if (worldTiles.Contains(new Vector2(x, y)))
                        {
                            GenerateTree(x, y + 1);
                        }
                    }
                }
            }
        }
    }
    private void GenerateNoiseTexture()
    {
        noiseTexture = new Texture2D(worldSize, worldSize);
        for(int x = 0; x < noiseTexture.width; x++)
        {
            for (int y = 0; y < noiseTexture.height; y++)
            {
                float v = Mathf.PerlinNoise((x + seed) * caveFreq, (y + seed) * caveFreq);
                noiseTexture.SetPixel(x, y, new Color(v, v, v));
            }
        }
        noiseTexture.Apply();
    }   
    void GenerateTree(int x, int y)
    {
        int treeHeight = Random.Range(minTreeHeight, maxTreeHeight);
        for (int i = 0; i < treeHeight; i++)
        {
            PlaceTile(tileAtlas.log.tileSprite, x, y + i);
        }

        PlaceTile(tileAtlas.leaves.tileSprite, x, y + treeHeight);
        PlaceTile(tileAtlas.leaves.tileSprite, x, y + treeHeight + 1);

        PlaceTile(tileAtlas.leaves.tileSprite, x + 2, y + treeHeight);
        PlaceTile(tileAtlas.leaves.tileSprite, x - 2, y + treeHeight);
        PlaceTile(tileAtlas.leaves.tileSprite, x + 1, y + treeHeight);
        PlaceTile(tileAtlas.leaves.tileSprite, x - 1, y + treeHeight);

        PlaceTile(tileAtlas.leaves.tileSprite, x + 1, y + treeHeight + 1);
        PlaceTile(tileAtlas.leaves.tileSprite, x - 1, y + treeHeight + 1);

        PlaceTile(tileAtlas.log.tileSprite, x, y + treeHeight + 2);

        PlaceTile(tileAtlas.leaves.tileSprite, x, y + treeHeight + 3);
        PlaceTile(tileAtlas.leaves.tileSprite, x, y + treeHeight + 4);

        PlaceTile(tileAtlas.leaves.tileSprite, x + 1, y + treeHeight + 3);
        PlaceTile(tileAtlas.leaves.tileSprite, x - 1, y + treeHeight + 3);
    }
    public void PlaceTile(Sprite tileSprite, int x, int y)
    {
        GameObject newTile = new GameObject();

        float chunkCoord = (Mathf.Round(x / chunkSize) * chunkSize);
        chunkCoord /= chunkSize;
        //Debug.Log(chunkCoord);
        newTile.transform.parent = worldChunks[(int)chunkCoord].transform;

        newTile.AddComponent<SpriteRenderer>();
        newTile.GetComponent<SpriteRenderer>().sprite = tileSprite;
        newTile.name = tileSprite.name;
        newTile.transform.position = new Vector2(x + .5f, y + .5f);
        worldTiles.Add(newTile.transform.position - (Vector3.one * .5f));
    }
}