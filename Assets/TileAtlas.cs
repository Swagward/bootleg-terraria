using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TileAtlas", menuName = "TileAtlas")]
public class TileAtlas : ScriptableObject
{
    public TileClass leaves;
    public TileClass log;
    public TileClass grass;
    public TileClass dirt;
    public TileClass stone;
}
